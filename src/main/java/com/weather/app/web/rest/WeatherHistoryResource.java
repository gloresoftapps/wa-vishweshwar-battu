package com.weather.app.web.rest;

import com.weather.app.domain.WeatherHistory;
import com.weather.app.service.WeatherHistoryService;
import com.weather.app.service.WeatherService;
import com.weather.app.service.dto.WeatherDTO;
import com.weather.app.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.weather.app.domain.WeatherHistory}.
 */
@RestController
@RequestMapping("/api")
public class WeatherHistoryResource {

    private final Logger log = LoggerFactory.getLogger(WeatherHistoryResource.class);

    private static final String ENTITY_NAME = "weatherHistory";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final WeatherHistoryService weatherHistoryService;
    
    private final WeatherService weatherService;

    public WeatherHistoryResource(WeatherHistoryService weatherHistoryService, WeatherService weatherService) {
        this.weatherHistoryService = weatherHistoryService;
        this.weatherService = weatherService;
    }

    /**
     * {@code POST  /weather-histories} : Create a new weatherHistory.
     *
     * @param weatherHistory the weatherHistory to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new weatherHistory, or with status {@code 400 (Bad Request)} if the weatherHistory has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/weather-histories")
    public ResponseEntity<WeatherHistory> createWeatherHistory(@RequestBody WeatherHistory weatherHistory) throws URISyntaxException {
        log.debug("REST request to save WeatherHistory : {}", weatherHistory);
        if (weatherHistory.getId() != null) {
            throw new BadRequestAlertException("A new weatherHistory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WeatherHistory result = weatherHistoryService.save(weatherHistory);
        return ResponseEntity.created(new URI("/api/weather-histories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /weather-histories} : Updates an existing weatherHistory.
     *
     * @param weatherHistory the weatherHistory to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated weatherHistory,
     * or with status {@code 400 (Bad Request)} if the weatherHistory is not valid,
     * or with status {@code 500 (Internal Server Error)} if the weatherHistory couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/weather-histories")
    public ResponseEntity<WeatherHistory> updateWeatherHistory(@RequestBody WeatherHistory weatherHistory) throws URISyntaxException {
        log.debug("REST request to update WeatherHistory : {}", weatherHistory);
        if (weatherHistory.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        WeatherHistory result = weatherHistoryService.save(weatherHistory);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, weatherHistory.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /weather-histories} : get all the weatherHistories.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of weatherHistories in body.
     */
    @GetMapping("/weather-histories")
    public List<WeatherHistory> getAllWeatherHistories() {
        log.debug("REST request to get all WeatherHistories");
        return weatherHistoryService.findAll();
    }
    
    /**
     * {@code GET  /weather-histories/:id} : get the "id" weatherHistory.
     *
     * @param city the city of the weatherHistory to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the weatherHistory, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/weather-histories/getWeather/{city}")
    public WeatherDTO getWeatherByCity(@PathVariable String city) {
        log.debug("REST request to get WeatherHistory : {}", city);
        return weatherService.getWeather(city);
    }

    /**
     * {@code GET  /weather-histories/:id} : get the "id" weatherHistory.
     *
     * @param id the id of the weatherHistory to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the weatherHistory, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/weather-histories/{id}")
    public ResponseEntity<WeatherHistory> getWeatherHistory(@PathVariable Long id) {
        log.debug("REST request to get WeatherHistory : {}", id);
        Optional<WeatherHistory> weatherHistory = weatherHistoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(weatherHistory);
    }

    /**
     * {@code DELETE  /weather-histories/:id} : delete the "id" weatherHistory.
     *
     * @param id the id of the weatherHistory to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/weather-histories/{id}")
    public ResponseEntity<Void> deleteWeatherHistory(@PathVariable Long id) {
        log.debug("REST request to delete WeatherHistory : {}", id);
        weatherHistoryService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
