package com.weather.app.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A WeatherHistory.
 */
@Entity
@Table(name = "weather_history")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class WeatherHistory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "city_name")
    private String cityName;

    @Column(name = "weather_description")
    private String weatherDescription;

    @Column(name = "current_temperature")
    private Double currentTemperature;

    @Column(name = "min_temperature")
    private Double minTemperature;

    @Column(name = "max_temperature")
    private Double maxTemperature;

    @Column(name = "sunset")
    private Double sunset;

    @Column(name = "sunrise")
    private LocalDate sunrise;

    @ManyToOne
    @JsonIgnoreProperties("weatherHistories")
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCityName() {
        return cityName;
    }

    public WeatherHistory cityName(String cityName) {
        this.cityName = cityName;
        return this;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getWeatherDescription() {
        return weatherDescription;
    }

    public WeatherHistory weatherDescription(String weatherDescription) {
        this.weatherDescription = weatherDescription;
        return this;
    }

    public void setWeatherDescription(String weatherDescription) {
        this.weatherDescription = weatherDescription;
    }

    public Double getCurrentTemperature() {
        return currentTemperature;
    }

    public WeatherHistory currentTemperature(Double currentTemperature) {
        this.currentTemperature = currentTemperature;
        return this;
    }

    public void setCurrentTemperature(Double currentTemperature) {
        this.currentTemperature = currentTemperature;
    }

    public Double getMinTemperature() {
        return minTemperature;
    }

    public WeatherHistory minTemperature(Double minTemperature) {
        this.minTemperature = minTemperature;
        return this;
    }

    public void setMinTemperature(Double minTemperature) {
        this.minTemperature = minTemperature;
    }

    public Double getMaxTemperature() {
        return maxTemperature;
    }

    public WeatherHistory maxTemperature(Double maxTemperature) {
        this.maxTemperature = maxTemperature;
        return this;
    }

    public void setMaxTemperature(Double maxTemperature) {
        this.maxTemperature = maxTemperature;
    }

    public Double getSunset() {
        return sunset;
    }

    public WeatherHistory sunset(Double sunset) {
        this.sunset = sunset;
        return this;
    }

    public void setSunset(Double sunset) {
        this.sunset = sunset;
    }

    public LocalDate getSunrise() {
        return sunrise;
    }

    public WeatherHistory sunrise(LocalDate sunrise) {
        this.sunrise = sunrise;
        return this;
    }

    public void setSunrise(LocalDate sunrise) {
        this.sunrise = sunrise;
    }

    public User getUser() {
        return user;
    }

    public WeatherHistory user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof WeatherHistory)) {
            return false;
        }
        return id != null && id.equals(((WeatherHistory) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "WeatherHistory{" +
            "id=" + getId() +
            ", cityName='" + getCityName() + "'" +
            ", weatherDescription='" + getWeatherDescription() + "'" +
            ", currentTemperature=" + getCurrentTemperature() +
            ", minTemperature=" + getMinTemperature() +
            ", maxTemperature=" + getMaxTemperature() +
            ", sunset=" + getSunset() +
            ", sunrise='" + getSunrise() + "'" +
            "}";
    }
}
