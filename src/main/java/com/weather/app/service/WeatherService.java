package com.weather.app.service;

import com.weather.app.service.dto.WeatherDTO;
import com.weather.app.domain.WeatherHistory;
import com.weather.app.repository.WeatherHistoryRepository;

import java.net.URI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriTemplate;

@Service
@Transactional
public class WeatherService {

    private static final Logger logger = LoggerFactory.getLogger(WeatherService.class);

    private final String apiUrl;

    /*    @Bean*/    private final RestTemplate restTemplate;

    private final String apiKey;

    private final WeatherHistoryRepository weatherHistoryRepository;
    
    public WeatherService (RestTemplateBuilder builder, WeatherHistoryRepository weatherHistoryRepository) {
            this.restTemplate = builder.build();
            this.weatherHistoryRepository = weatherHistoryRepository;
            //this.restTemplate = restTemplate;
            //this.restTemplate.setErrorHandler(new OWMResponseErrorHandler());
            this.apiKey = "f66ce3d7de2c1cadf0bab5b3414e3c4d";
            this.apiUrl = "http://api.openweathermap.org/data/2.5/weather?q={city}&APPID={key}";
    }

    //@Cacheable("weatherDTO")
    public WeatherDTO getWeather(String city){
            logger.debug("Requesting current weather for {}", city);
            logger.debug("API key {} , url {}", apiKey, apiUrl);
            WeatherDTO weatherDTO = null;
            if(validParameters(city)) {
                    URI url = new UriTemplate(this.apiUrl).expand(city, this.apiKey);

                    weatherDTO = invoke(url, WeatherDTO.class);
                    WeatherHistory weatherHistory = new WeatherHistory();
                    weatherHistory.setCityName(weatherDTO.getName());
                    weatherHistory.setCurrentTemperature(weatherDTO.getTemperature());
                    weatherHistory.setMaxTemperature(weatherDTO.getTemp_max());
                    weatherHistory.setMinTemperature(weatherDTO.getTemp_min());
                    weatherHistory.setWeatherDescription(weatherDTO.getWeatherDescription());
                    //weatherHistory.setSunrise(weatherDTO.getSunrise());
                    weatherHistory.setSunset((double)weatherDTO.getSunset());
                    // add other properties
                    weatherHistoryRepository.save(weatherHistory);
            }
            return weatherDTO;
    }

    private boolean validParameters(String city) {
            return city !=null && !"".equals(city) && apiKey !=null && !"".equals(apiKey) && apiUrl!=null && !"".equals(apiUrl);
    }

    private <T> T invoke(URI url, Class<T> responseType){
            T weather = null;
            try {
                    RequestEntity<?> request = RequestEntity.get(url)
                                    .accept(MediaType.APPLICATION_JSON).build();
                    ResponseEntity<T> exchange = this.restTemplate
                                    .exchange(request, responseType);
                    weather = exchange.getBody();
            } catch(Exception e){
                            logger.error("An error occurred while calling openweathermap.org API endpoint:  " + e.getMessage());
            }

            return weather;
    }

}
