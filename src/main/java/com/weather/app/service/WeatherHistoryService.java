package com.weather.app.service;

import com.weather.app.domain.WeatherHistory;
import com.weather.app.repository.WeatherHistoryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link WeatherHistory}.
 */
@Service
@Transactional
public class WeatherHistoryService {

    private final Logger log = LoggerFactory.getLogger(WeatherHistoryService.class);

    private final WeatherHistoryRepository weatherHistoryRepository;

    public WeatherHistoryService(WeatherHistoryRepository weatherHistoryRepository) {
        this.weatherHistoryRepository = weatherHistoryRepository;
    }

    /**
     * Save a weatherHistory.
     *
     * @param weatherHistory the entity to save.
     * @return the persisted entity.
     */
    public WeatherHistory save(WeatherHistory weatherHistory) {
        log.debug("Request to save WeatherHistory : {}", weatherHistory);
        return weatherHistoryRepository.save(weatherHistory);
    }

    /**
     * Get all the weatherHistories.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<WeatherHistory> findAll() {
        log.debug("Request to get all WeatherHistories");
        return weatherHistoryRepository.findAll();
    }


    /**
     * Get one weatherHistory by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<WeatherHistory> findOne(Long id) {
        log.debug("Request to get WeatherHistory : {}", id);
        return weatherHistoryRepository.findById(id);
    }

    /**
     * Delete the weatherHistory by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete WeatherHistory : {}", id);
        weatherHistoryRepository.deleteById(id);
    }
}
