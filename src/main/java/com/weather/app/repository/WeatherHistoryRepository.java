package com.weather.app.repository;

import com.weather.app.domain.WeatherHistory;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the WeatherHistory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WeatherHistoryRepository extends JpaRepository<WeatherHistory, Long> {

    @Query("select weatherHistory from WeatherHistory weatherHistory where weatherHistory.user.login = ?#{principal.username}")
    List<WeatherHistory> findByUserIsCurrentUser();

}
