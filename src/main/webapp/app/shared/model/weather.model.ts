export interface IWeather {
  id?: number;
  name?: string;
}

export class Weather implements IWeather {
  constructor(public id?: number, public name?: string) {}
}
