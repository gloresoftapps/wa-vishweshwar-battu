import { Moment } from 'moment';
import { IUser } from 'app/core/user/user.model';

export interface IWeatherHistory {
  id?: number;
  cityName?: string;
  weatherDescription?: string;
  currentTemperature?: number;
  minTemperature?: number;
  maxTemperature?: number;
  sunset?: number;
  sunrise?: Moment;
  user?: IUser;
}

export interface IWeather {
  weatherId?: number;
  name?: string;
  weatherDescription?: string;
  temperature?: number;
  min_Temperature?: number;
  max_Temperature?: number;
  sunset?: number;
  sunrise?: Moment;
}

export class WeatherHistory implements IWeatherHistory {
  constructor(
    public id?: number,
    public cityName?: string,
    public weatherDescription?: string,
    public currentTemperature?: number,
    public minTemperature?: number,
    public maxTemperature?: number,
    public sunset?: number,
    public sunrise?: Moment,
    public user?: IUser
  ) {}
}
