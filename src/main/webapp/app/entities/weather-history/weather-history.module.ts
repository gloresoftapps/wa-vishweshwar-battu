import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { WeatherAppSharedModule } from 'app/shared/shared.module';
import { WeatherHistoryComponent } from './weather-history.component';
import { WeatherHistoryDetailComponent } from './weather-history-detail.component';
import { WeatherHistoryUpdateComponent } from './weather-history-update.component';
import { WeatherHistoryDeleteDialogComponent } from './weather-history-delete-dialog.component';
import { weatherHistoryRoute } from './weather-history.route';

@NgModule({
  imports: [WeatherAppSharedModule, RouterModule.forChild(weatherHistoryRoute)],
  declarations: [
    WeatherHistoryComponent,
    WeatherHistoryDetailComponent,
    WeatherHistoryUpdateComponent,
    WeatherHistoryDeleteDialogComponent
  ],
  entryComponents: [WeatherHistoryDeleteDialogComponent]
})
export class WeatherAppWeatherHistoryModule {}
