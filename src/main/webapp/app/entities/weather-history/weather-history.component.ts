import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IWeatherHistory, IWeather } from 'app/shared/model/weather-history.model';
import { WeatherHistoryService } from './weather-history.service';
import { WeatherHistoryDeleteDialogComponent } from './weather-history-delete-dialog.component';

@Component({
  selector: 'jhi-weather-history',
  templateUrl: './weather-history.component.html'
})
export class WeatherHistoryComponent implements OnInit, OnDestroy {
  weatherHistories?: IWeatherHistory[];
  eventSubscriber?: Subscription;

  weather?: IWeather;
  city: string;

  constructor(
    protected weatherHistoryService: WeatherHistoryService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {
    this.city = '';
  }

  loadAll(): void {
    this.weatherHistoryService.query().subscribe((res: HttpResponse<IWeatherHistory[]>) => {
      this.weatherHistories = res.body ? res.body : [];
    });
  }

  getWeatherByCity(): void {
    this.weatherHistoryService.findWeather(this.city).subscribe((res: HttpResponse<IWeather>) => {
      this.weather = res.body ? res.body : {};
    });
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInWeatherHistories();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IWeatherHistory): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInWeatherHistories(): void {
    this.eventSubscriber = this.eventManager.subscribe('weatherHistoryListModification', () => this.loadAll());
  }

  delete(weatherHistory: IWeatherHistory): void {
    const modalRef = this.modalService.open(WeatherHistoryDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.weatherHistory = weatherHistory;
  }
}
