import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IWeatherHistory } from 'app/shared/model/weather-history.model';

@Component({
  selector: 'jhi-weather-history-detail',
  templateUrl: './weather-history-detail.component.html'
})
export class WeatherHistoryDetailComponent implements OnInit {
  weatherHistory: IWeatherHistory | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ weatherHistory }) => {
      this.weatherHistory = weatherHistory;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
