import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IWeatherHistory, IWeather } from 'app/shared/model/weather-history.model';

type EntityResponseType = HttpResponse<IWeatherHistory>;
type EntityArrayResponseType = HttpResponse<IWeatherHistory[]>;

@Injectable({ providedIn: 'root' })
export class WeatherHistoryService {
  public resourceUrl = SERVER_API_URL + 'api/weather-histories';
  public resourceWeatherUrl = SERVER_API_URL + 'api/getWeather';

  constructor(protected http: HttpClient) {}

  create(weatherHistory: IWeatherHistory): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(weatherHistory);
    return this.http
      .post<IWeatherHistory>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(weatherHistory: IWeatherHistory): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(weatherHistory);
    return this.http
      .put<IWeatherHistory>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IWeatherHistory>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  findWeather(city: string): Observable<EntityResponseType> {
    return this.http.get<IWeather>(`${this.resourceUrl}/getWeather/${city}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IWeatherHistory[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(weatherHistory: IWeatherHistory): IWeatherHistory {
    const copy: IWeatherHistory = Object.assign({}, weatherHistory, {
      sunrise: weatherHistory.sunrise && weatherHistory.sunrise.isValid() ? weatherHistory.sunrise.format(DATE_FORMAT) : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.sunrise = res.body.sunrise ? moment(res.body.sunrise) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((weatherHistory: IWeatherHistory) => {
        weatherHistory.sunrise = weatherHistory.sunrise ? moment(weatherHistory.sunrise) : undefined;
      });
    }
    return res;
  }
}
