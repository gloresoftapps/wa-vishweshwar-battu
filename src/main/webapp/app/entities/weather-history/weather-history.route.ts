import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IWeatherHistory, WeatherHistory } from 'app/shared/model/weather-history.model';
import { WeatherHistoryService } from './weather-history.service';
import { WeatherHistoryComponent } from './weather-history.component';
import { WeatherHistoryDetailComponent } from './weather-history-detail.component';
import { WeatherHistoryUpdateComponent } from './weather-history-update.component';

@Injectable({ providedIn: 'root' })
export class WeatherHistoryResolve implements Resolve<IWeatherHistory> {
  constructor(private service: WeatherHistoryService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IWeatherHistory> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((weatherHistory: HttpResponse<WeatherHistory>) => {
          if (weatherHistory.body) {
            return of(weatherHistory.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new WeatherHistory());
  }
}

export const weatherHistoryRoute: Routes = [
  {
    path: '',
    component: WeatherHistoryComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'WeatherHistories'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: WeatherHistoryDetailComponent,
    resolve: {
      weatherHistory: WeatherHistoryResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'WeatherHistories'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: WeatherHistoryUpdateComponent,
    resolve: {
      weatherHistory: WeatherHistoryResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'WeatherHistories'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: WeatherHistoryUpdateComponent,
    resolve: {
      weatherHistory: WeatherHistoryResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'WeatherHistories'
    },
    canActivate: [UserRouteAccessService]
  }
];
