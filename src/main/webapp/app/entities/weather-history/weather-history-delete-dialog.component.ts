import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IWeatherHistory } from 'app/shared/model/weather-history.model';
import { WeatherHistoryService } from './weather-history.service';

@Component({
  templateUrl: './weather-history-delete-dialog.component.html'
})
export class WeatherHistoryDeleteDialogComponent {
  weatherHistory?: IWeatherHistory;

  constructor(
    protected weatherHistoryService: WeatherHistoryService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.weatherHistoryService.delete(id).subscribe(() => {
      this.eventManager.broadcast('weatherHistoryListModification');
      this.activeModal.close();
    });
  }
}
