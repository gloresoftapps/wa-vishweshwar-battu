import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { IWeatherHistory, WeatherHistory } from 'app/shared/model/weather-history.model';
import { WeatherHistoryService } from './weather-history.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';

@Component({
  selector: 'jhi-weather-history-update',
  templateUrl: './weather-history-update.component.html'
})
export class WeatherHistoryUpdateComponent implements OnInit {
  isSaving = false;

  users: IUser[] = [];
  sunriseDp: any;

  editForm = this.fb.group({
    id: [],
    cityName: [],
    weatherDescription: [],
    currentTemperature: [],
    minTemperature: [],
    maxTemperature: [],
    sunset: [],
    sunrise: [],
    user: []
  });

  constructor(
    protected weatherHistoryService: WeatherHistoryService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ weatherHistory }) => {
      this.updateForm(weatherHistory);

      this.userService
        .query()
        .pipe(
          map((res: HttpResponse<IUser[]>) => {
            return res.body ? res.body : [];
          })
        )
        .subscribe((resBody: IUser[]) => (this.users = resBody));
    });
  }

  updateForm(weatherHistory: IWeatherHistory): void {
    this.editForm.patchValue({
      id: weatherHistory.id,
      cityName: weatherHistory.cityName,
      weatherDescription: weatherHistory.weatherDescription,
      currentTemperature: weatherHistory.currentTemperature,
      minTemperature: weatherHistory.minTemperature,
      maxTemperature: weatherHistory.maxTemperature,
      sunset: weatherHistory.sunset,
      sunrise: weatherHistory.sunrise,
      user: weatherHistory.user
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const weatherHistory = this.createFromForm();
    if (weatherHistory.id !== undefined) {
      this.subscribeToSaveResponse(this.weatherHistoryService.update(weatherHistory));
    } else {
      this.subscribeToSaveResponse(this.weatherHistoryService.create(weatherHistory));
    }
  }

  private createFromForm(): IWeatherHistory {
    return {
      ...new WeatherHistory(),
      id: this.editForm.get(['id'])!.value,
      cityName: this.editForm.get(['cityName'])!.value,
      weatherDescription: this.editForm.get(['weatherDescription'])!.value,
      currentTemperature: this.editForm.get(['currentTemperature'])!.value,
      minTemperature: this.editForm.get(['minTemperature'])!.value,
      maxTemperature: this.editForm.get(['maxTemperature'])!.value,
      sunset: this.editForm.get(['sunset'])!.value,
      sunrise: this.editForm.get(['sunrise'])!.value,
      user: this.editForm.get(['user'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IWeatherHistory>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IUser): any {
    return item.id;
  }
}
