import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IWeather, Weather } from 'app/shared/model/weather.model';
import { WeatherService } from './weather.service';

@Component({
  selector: 'jhi-weather-update',
  templateUrl: './weather-update.component.html'
})
export class WeatherUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: []
  });

  constructor(protected weatherService: WeatherService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ weather }) => {
      this.updateForm(weather);
    });
  }

  updateForm(weather: IWeather): void {
    this.editForm.patchValue({
      id: weather.id,
      name: weather.name
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const weather = this.createFromForm();
    if (weather.id !== undefined) {
      this.subscribeToSaveResponse(this.weatherService.update(weather));
    } else {
      this.subscribeToSaveResponse(this.weatherService.create(weather));
    }
  }

  private createFromForm(): IWeather {
    return {
      ...new Weather(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IWeather>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
