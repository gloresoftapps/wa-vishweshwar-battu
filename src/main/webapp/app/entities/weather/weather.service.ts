import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IWeather } from 'app/shared/model/weather.model';

type EntityResponseType = HttpResponse<IWeather>;
type EntityArrayResponseType = HttpResponse<IWeather[]>;

@Injectable({ providedIn: 'root' })
export class WeatherService {
  public resourceUrl = SERVER_API_URL + 'api/weathers';

  constructor(protected http: HttpClient) {}

  create(weather: IWeather): Observable<EntityResponseType> {
    return this.http.post<IWeather>(this.resourceUrl, weather, { observe: 'response' });
  }

  update(weather: IWeather): Observable<EntityResponseType> {
    return this.http.put<IWeather>(this.resourceUrl, weather, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IWeather>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IWeather[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
