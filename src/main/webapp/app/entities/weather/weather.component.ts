import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IWeather } from 'app/shared/model/weather.model';
import { WeatherService } from './weather.service';
import { WeatherDeleteDialogComponent } from './weather-delete-dialog.component';

@Component({
  selector: 'jhi-weather',
  templateUrl: './weather.component.html'
})
export class WeatherComponent implements OnInit, OnDestroy {
  weathers?: IWeather[];
  eventSubscriber?: Subscription;

  constructor(protected weatherService: WeatherService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.weatherService.query().subscribe((res: HttpResponse<IWeather[]>) => {
      this.weathers = res.body ? res.body : [];
    });
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInWeathers();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IWeather): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInWeathers(): void {
    this.eventSubscriber = this.eventManager.subscribe('weatherListModification', () => this.loadAll());
  }

  delete(weather: IWeather): void {
    const modalRef = this.modalService.open(WeatherDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.weather = weather;
  }
}
