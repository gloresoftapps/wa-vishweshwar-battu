package com.weather.app.web.rest;

import com.weather.app.WeatherApp;
import com.weather.app.domain.WeatherHistory;
import com.weather.app.repository.WeatherHistoryRepository;
import com.weather.app.service.WeatherHistoryService;
import com.weather.app.service.WeatherService;
import com.weather.app.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.weather.app.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link WeatherHistoryResource} REST controller.
 */
@SpringBootTest(classes = WeatherApp.class)
public class WeatherHistoryResourceIT {

    private static final String DEFAULT_CITY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CITY_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_WEATHER_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_WEATHER_DESCRIPTION = "BBBBBBBBBB";

    private static final Double DEFAULT_CURRENT_TEMPERATURE = 1D;
    private static final Double UPDATED_CURRENT_TEMPERATURE = 2D;

    private static final Double DEFAULT_MIN_TEMPERATURE = 1D;
    private static final Double UPDATED_MIN_TEMPERATURE = 2D;

    private static final Double DEFAULT_MAX_TEMPERATURE = 1D;
    private static final Double UPDATED_MAX_TEMPERATURE = 2D;

    private static final Double DEFAULT_SUNSET = 1D;
    private static final Double UPDATED_SUNSET = 2D;

    private static final LocalDate DEFAULT_SUNRISE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_SUNRISE = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private WeatherHistoryRepository weatherHistoryRepository;

    @Autowired
    private WeatherHistoryService weatherHistoryService;
    
    @Autowired
    private WeatherService weatherService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restWeatherHistoryMockMvc;

    private WeatherHistory weatherHistory;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final WeatherHistoryResource weatherHistoryResource = new WeatherHistoryResource(weatherHistoryService, weatherService);
        this.restWeatherHistoryMockMvc = MockMvcBuilders.standaloneSetup(weatherHistoryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static WeatherHistory createEntity(EntityManager em) {
        WeatherHistory weatherHistory = new WeatherHistory()
            .cityName(DEFAULT_CITY_NAME)
            .weatherDescription(DEFAULT_WEATHER_DESCRIPTION)
            .currentTemperature(DEFAULT_CURRENT_TEMPERATURE)
            .minTemperature(DEFAULT_MIN_TEMPERATURE)
            .maxTemperature(DEFAULT_MAX_TEMPERATURE)
            .sunset(DEFAULT_SUNSET)
            .sunrise(DEFAULT_SUNRISE);
        return weatherHistory;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static WeatherHistory createUpdatedEntity(EntityManager em) {
        WeatherHistory weatherHistory = new WeatherHistory()
            .cityName(UPDATED_CITY_NAME)
            .weatherDescription(UPDATED_WEATHER_DESCRIPTION)
            .currentTemperature(UPDATED_CURRENT_TEMPERATURE)
            .minTemperature(UPDATED_MIN_TEMPERATURE)
            .maxTemperature(UPDATED_MAX_TEMPERATURE)
            .sunset(UPDATED_SUNSET)
            .sunrise(UPDATED_SUNRISE);
        return weatherHistory;
    }

    @BeforeEach
    public void initTest() {
        weatherHistory = createEntity(em);
    }

    @Test
    @Transactional
    public void createWeatherHistory() throws Exception {
        int databaseSizeBeforeCreate = weatherHistoryRepository.findAll().size();

        // Create the WeatherHistory
        restWeatherHistoryMockMvc.perform(post("/api/weather-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(weatherHistory)))
            .andExpect(status().isCreated());

        // Validate the WeatherHistory in the database
        List<WeatherHistory> weatherHistoryList = weatherHistoryRepository.findAll();
        assertThat(weatherHistoryList).hasSize(databaseSizeBeforeCreate + 1);
        WeatherHistory testWeatherHistory = weatherHistoryList.get(weatherHistoryList.size() - 1);
        assertThat(testWeatherHistory.getCityName()).isEqualTo(DEFAULT_CITY_NAME);
        assertThat(testWeatherHistory.getWeatherDescription()).isEqualTo(DEFAULT_WEATHER_DESCRIPTION);
        assertThat(testWeatherHistory.getCurrentTemperature()).isEqualTo(DEFAULT_CURRENT_TEMPERATURE);
        assertThat(testWeatherHistory.getMinTemperature()).isEqualTo(DEFAULT_MIN_TEMPERATURE);
        assertThat(testWeatherHistory.getMaxTemperature()).isEqualTo(DEFAULT_MAX_TEMPERATURE);
        assertThat(testWeatherHistory.getSunset()).isEqualTo(DEFAULT_SUNSET);
        assertThat(testWeatherHistory.getSunrise()).isEqualTo(DEFAULT_SUNRISE);
    }

    @Test
    @Transactional
    public void createWeatherHistoryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = weatherHistoryRepository.findAll().size();

        // Create the WeatherHistory with an existing ID
        weatherHistory.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restWeatherHistoryMockMvc.perform(post("/api/weather-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(weatherHistory)))
            .andExpect(status().isBadRequest());

        // Validate the WeatherHistory in the database
        List<WeatherHistory> weatherHistoryList = weatherHistoryRepository.findAll();
        assertThat(weatherHistoryList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllWeatherHistories() throws Exception {
        // Initialize the database
        weatherHistoryRepository.saveAndFlush(weatherHistory);

        // Get all the weatherHistoryList
        restWeatherHistoryMockMvc.perform(get("/api/weather-histories?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(weatherHistory.getId().intValue())))
            .andExpect(jsonPath("$.[*].cityName").value(hasItem(DEFAULT_CITY_NAME)))
            .andExpect(jsonPath("$.[*].weatherDescription").value(hasItem(DEFAULT_WEATHER_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].currentTemperature").value(hasItem(DEFAULT_CURRENT_TEMPERATURE.doubleValue())))
            .andExpect(jsonPath("$.[*].minTemperature").value(hasItem(DEFAULT_MIN_TEMPERATURE.doubleValue())))
            .andExpect(jsonPath("$.[*].maxTemperature").value(hasItem(DEFAULT_MAX_TEMPERATURE.doubleValue())))
            .andExpect(jsonPath("$.[*].sunset").value(hasItem(DEFAULT_SUNSET.doubleValue())))
            .andExpect(jsonPath("$.[*].sunrise").value(hasItem(DEFAULT_SUNRISE.toString())));
    }
    
    @Test
    @Transactional
    public void getWeatherHistory() throws Exception {
        // Initialize the database
        weatherHistoryRepository.saveAndFlush(weatherHistory);

        // Get the weatherHistory
        restWeatherHistoryMockMvc.perform(get("/api/weather-histories/{id}", weatherHistory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(weatherHistory.getId().intValue()))
            .andExpect(jsonPath("$.cityName").value(DEFAULT_CITY_NAME))
            .andExpect(jsonPath("$.weatherDescription").value(DEFAULT_WEATHER_DESCRIPTION))
            .andExpect(jsonPath("$.currentTemperature").value(DEFAULT_CURRENT_TEMPERATURE.doubleValue()))
            .andExpect(jsonPath("$.minTemperature").value(DEFAULT_MIN_TEMPERATURE.doubleValue()))
            .andExpect(jsonPath("$.maxTemperature").value(DEFAULT_MAX_TEMPERATURE.doubleValue()))
            .andExpect(jsonPath("$.sunset").value(DEFAULT_SUNSET.doubleValue()))
            .andExpect(jsonPath("$.sunrise").value(DEFAULT_SUNRISE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingWeatherHistory() throws Exception {
        // Get the weatherHistory
        restWeatherHistoryMockMvc.perform(get("/api/weather-histories/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateWeatherHistory() throws Exception {
        // Initialize the database
        weatherHistoryService.save(weatherHistory);

        int databaseSizeBeforeUpdate = weatherHistoryRepository.findAll().size();

        // Update the weatherHistory
        WeatherHistory updatedWeatherHistory = weatherHistoryRepository.findById(weatherHistory.getId()).get();
        // Disconnect from session so that the updates on updatedWeatherHistory are not directly saved in db
        em.detach(updatedWeatherHistory);
        updatedWeatherHistory
            .cityName(UPDATED_CITY_NAME)
            .weatherDescription(UPDATED_WEATHER_DESCRIPTION)
            .currentTemperature(UPDATED_CURRENT_TEMPERATURE)
            .minTemperature(UPDATED_MIN_TEMPERATURE)
            .maxTemperature(UPDATED_MAX_TEMPERATURE)
            .sunset(UPDATED_SUNSET)
            .sunrise(UPDATED_SUNRISE);

        restWeatherHistoryMockMvc.perform(put("/api/weather-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedWeatherHistory)))
            .andExpect(status().isOk());

        // Validate the WeatherHistory in the database
        List<WeatherHistory> weatherHistoryList = weatherHistoryRepository.findAll();
        assertThat(weatherHistoryList).hasSize(databaseSizeBeforeUpdate);
        WeatherHistory testWeatherHistory = weatherHistoryList.get(weatherHistoryList.size() - 1);
        assertThat(testWeatherHistory.getCityName()).isEqualTo(UPDATED_CITY_NAME);
        assertThat(testWeatherHistory.getWeatherDescription()).isEqualTo(UPDATED_WEATHER_DESCRIPTION);
        assertThat(testWeatherHistory.getCurrentTemperature()).isEqualTo(UPDATED_CURRENT_TEMPERATURE);
        assertThat(testWeatherHistory.getMinTemperature()).isEqualTo(UPDATED_MIN_TEMPERATURE);
        assertThat(testWeatherHistory.getMaxTemperature()).isEqualTo(UPDATED_MAX_TEMPERATURE);
        assertThat(testWeatherHistory.getSunset()).isEqualTo(UPDATED_SUNSET);
        assertThat(testWeatherHistory.getSunrise()).isEqualTo(UPDATED_SUNRISE);
    }

    @Test
    @Transactional
    public void updateNonExistingWeatherHistory() throws Exception {
        int databaseSizeBeforeUpdate = weatherHistoryRepository.findAll().size();

        // Create the WeatherHistory

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restWeatherHistoryMockMvc.perform(put("/api/weather-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(weatherHistory)))
            .andExpect(status().isBadRequest());

        // Validate the WeatherHistory in the database
        List<WeatherHistory> weatherHistoryList = weatherHistoryRepository.findAll();
        assertThat(weatherHistoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteWeatherHistory() throws Exception {
        // Initialize the database
        weatherHistoryService.save(weatherHistory);

        int databaseSizeBeforeDelete = weatherHistoryRepository.findAll().size();

        // Delete the weatherHistory
        restWeatherHistoryMockMvc.perform(delete("/api/weather-histories/{id}", weatherHistory.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<WeatherHistory> weatherHistoryList = weatherHistoryRepository.findAll();
        assertThat(weatherHistoryList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
