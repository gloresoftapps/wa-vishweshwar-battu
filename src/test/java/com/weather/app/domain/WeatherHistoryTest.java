package com.weather.app.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.weather.app.web.rest.TestUtil;

public class WeatherHistoryTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(WeatherHistory.class);
        WeatherHistory weatherHistory1 = new WeatherHistory();
        weatherHistory1.setId(1L);
        WeatherHistory weatherHistory2 = new WeatherHistory();
        weatherHistory2.setId(weatherHistory1.getId());
        assertThat(weatherHistory1).isEqualTo(weatherHistory2);
        weatherHistory2.setId(2L);
        assertThat(weatherHistory1).isNotEqualTo(weatherHistory2);
        weatherHistory1.setId(null);
        assertThat(weatherHistory1).isNotEqualTo(weatherHistory2);
    }
}
