import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { WeatherHistoryService } from 'app/entities/weather-history/weather-history.service';
import { IWeatherHistory, WeatherHistory } from 'app/shared/model/weather-history.model';

describe('Service Tests', () => {
  describe('WeatherHistory Service', () => {
    let injector: TestBed;
    let service: WeatherHistoryService;
    let httpMock: HttpTestingController;
    let elemDefault: IWeatherHistory;
    let expectedResult: IWeatherHistory | IWeatherHistory[] | boolean | null;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(WeatherHistoryService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new WeatherHistory(0, 'AAAAAAA', 'AAAAAAA', 0, 0, 0, 0, currentDate);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            sunrise: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a WeatherHistory', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            sunrise: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            sunrise: currentDate
          },
          returnedFromService
        );
        service
          .create(new WeatherHistory())
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp.body));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a WeatherHistory', () => {
        const returnedFromService = Object.assign(
          {
            cityName: 'BBBBBB',
            weatherDescription: 'BBBBBB',
            currentTemperature: 1,
            minTemperature: 1,
            maxTemperature: 1,
            sunset: 1,
            sunrise: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            sunrise: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp.body));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of WeatherHistory', () => {
        const returnedFromService = Object.assign(
          {
            cityName: 'BBBBBB',
            weatherDescription: 'BBBBBB',
            currentTemperature: 1,
            minTemperature: 1,
            maxTemperature: 1,
            sunset: 1,
            sunrise: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            sunrise: currentDate
          },
          returnedFromService
        );
        service
          .query()
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a WeatherHistory', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
