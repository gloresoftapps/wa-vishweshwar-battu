import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { WeatherAppTestModule } from '../../../test.module';
import { WeatherHistoryDetailComponent } from 'app/entities/weather-history/weather-history-detail.component';
import { WeatherHistory } from 'app/shared/model/weather-history.model';

describe('Component Tests', () => {
  describe('WeatherHistory Management Detail Component', () => {
    let comp: WeatherHistoryDetailComponent;
    let fixture: ComponentFixture<WeatherHistoryDetailComponent>;
    const route = ({ data: of({ weatherHistory: new WeatherHistory(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WeatherAppTestModule],
        declarations: [WeatherHistoryDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(WeatherHistoryDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(WeatherHistoryDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load weatherHistory on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.weatherHistory).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
