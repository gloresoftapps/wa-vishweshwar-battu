import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { WeatherAppTestModule } from '../../../test.module';
import { WeatherHistoryComponent } from 'app/entities/weather-history/weather-history.component';
import { WeatherHistoryService } from 'app/entities/weather-history/weather-history.service';
import { WeatherHistory } from 'app/shared/model/weather-history.model';

describe('Component Tests', () => {
  describe('WeatherHistory Management Component', () => {
    let comp: WeatherHistoryComponent;
    let fixture: ComponentFixture<WeatherHistoryComponent>;
    let service: WeatherHistoryService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WeatherAppTestModule],
        declarations: [WeatherHistoryComponent],
        providers: []
      })
        .overrideTemplate(WeatherHistoryComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(WeatherHistoryComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(WeatherHistoryService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new WeatherHistory(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.weatherHistories && comp.weatherHistories[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
