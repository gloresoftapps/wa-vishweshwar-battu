import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { WeatherAppTestModule } from '../../../test.module';
import { WeatherHistoryUpdateComponent } from 'app/entities/weather-history/weather-history-update.component';
import { WeatherHistoryService } from 'app/entities/weather-history/weather-history.service';
import { WeatherHistory } from 'app/shared/model/weather-history.model';

describe('Component Tests', () => {
  describe('WeatherHistory Management Update Component', () => {
    let comp: WeatherHistoryUpdateComponent;
    let fixture: ComponentFixture<WeatherHistoryUpdateComponent>;
    let service: WeatherHistoryService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WeatherAppTestModule],
        declarations: [WeatherHistoryUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(WeatherHistoryUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(WeatherHistoryUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(WeatherHistoryService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new WeatherHistory(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new WeatherHistory();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
